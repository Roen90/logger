package com.ks.pruebas;

/**
 * Created by Miguel on 30/03/2016.
 */

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Bar {
    static final Logger logger = LogManager.getLogger(Bar.class.getName());
    private static String mensaje;

    public static String getMensaje() {
        return mensaje;
    }

    public static void setMensaje(String mensaje) {
        Bar.mensaje = mensaje;
    }

    public boolean doIt() {
        logger.entry();
        logger.info("Esto es INFO: " + mensaje);
        logger.error("Did it again!");
        return logger.exit(false);
    }
}
